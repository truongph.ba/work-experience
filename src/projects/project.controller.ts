import {
  Body,
  Controller, Delete,
  Get,
  HttpException,
  HttpStatus, Param,
  Post, Put
} from "@nestjs/common";
import { ProjectService } from './project.service';
import { CreateProjectDto } from './dto/create-project.dto';
import { ApiBadRequestResponse, ApiOkResponse, ApiTags } from "@nestjs/swagger";
import { UpdateProjectDto } from "./dto/update-project.dto";
@ApiTags('Projects')
@Controller('projects')
export class ProjectController {
  constructor(private projectService: ProjectService) {}
  @Get()
  @ApiOkResponse({ description: 'List all projects' })
  @ApiBadRequestResponse({ description: 'Get list all project error' })
  async findAll() {
    try {
      const data = await this.projectService.findAll();
      return {
        success: true,
        data,
      };
    } catch (e) {
      throw new HttpException(e, HttpStatus.BAD_REQUEST);
    }
  }

  @Post()
  async create(@Body() createProjectDto: CreateProjectDto) {
    try {
      const data = await this.projectService.create(createProjectDto);
      return {
        success: true,
        data,
      };
    } catch (e) {
      throw new HttpException(e, HttpStatus.BAD_REQUEST);
    }
  }
  @Put(':id')
  async update(
    @Param('id') id: string,
    @Body() updateProjectDto: UpdateProjectDto,
  ) {
    try {
      const data = await this.projectService.update(id, updateProjectDto);
      return {
        success: true,
        data,
      };
    } catch (e) {
      throw new HttpException(e, HttpStatus.BAD_REQUEST);
    }
  }
  @Delete(':id')
  async delete(@Param('id') id: string) {
    try {
      const data = await this.projectService.delete(id);
      return {
        success: true,
        data,
      };
    } catch (e) {
      throw new HttpException(e, HttpStatus.BAD_REQUEST);
    }
  }
}
