import { IsString, MinLength } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
export class CreateProjectDto {
  @ApiProperty()
  @IsString()
  @MinLength(5)
  name: string;
  @ApiProperty()
  @IsString()
  description: string;
}
