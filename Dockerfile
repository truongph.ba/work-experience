FROM  node:16.15-alpine

WORKDIR /app

RUN yarn global add @nestjs/cli

COPY .env ./

COPY package.json yarn.lock ./

RUN yarn --pure-lockfile

COPY . .

RUN yarn build

CMD ["yarn", "start:prod"]

